# exercice-login-auth
### révision Auth ( login & register )
1. Cloner le projet
2. Faire un composer install
3. Créer le .env.local avec la DATABASE_URL dedans
4.  Faire un bin/console do:da:cr pour créer la bdd si elle n'existe pas
5.  Faire un bin/console do:mi:ma
6. Faire un bin/console do:mi:mi pour exécuter les dernière migrations et mettre à jour la base de données
7. si ça marche pas un bin/console do:da:dr --force puis re commencer à partir de la 4ème étape . 